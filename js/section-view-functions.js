$(document).ready(function() {
    // HIDE SECTION INFO PASSIVES
    function sortSection() {
        $(".formating-info").hide();
        $(".active").show();
    }
    sortSection();

    // PASS ACTIVE CLASS TO REQUIRED ARTICLE
    $("a.view").click(function(event) {
        event.preventDefault();
    });
    $(".section-content .view").click(function() {
        var $recipient = $(this).attr('class').split(' ')[1];
        $(".active").removeClass("active");
        $("."+$recipient).addClass("active");
        sortSection();
    });
});